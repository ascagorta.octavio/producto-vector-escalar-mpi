#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#define MASTER 0

/*************************************************************************************************************
| - Uso:                                                                                                     |
|       Setear el valor de la variable "vector" y "escalar". Compilar usando: mpicc <fuente> -o <ejecutable>.|
|       Ejecutar usando: mpirun -np <nro de procesos> <ejecutable>                                           |
*************************************************************************************************************/

int main(int argc, char *argv[])

{
    /* Valor del vector y escalar*/
    int vector[] = {4,1};
    int escalar = 1;
    
    /* Datos */
    int size_vector = sizeof(vector)/sizeof(vector[0]);
    int result[size_vector];
    int count;

    /* MPI */
    int rank, size;
    MPI_Status status;
    
    /* Inicializacion MPI */
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if(rank != MASTER)
    {   
        MPI_Recv(&result, size_vector, MPI_INT, rank-1, 1, MPI_COMM_WORLD, &status);
    }

    /* Realiza el producto de los elementos asignados */
    count = rank;
    while(count < size_vector){
        result[count] = vector[count] * escalar;
        printf("\nsoy el proceso %d y resolví el elemento %d del vector resultante, resultado: %d \n",rank, count, result[count]);
        count = count + size;
    }

    /* MASTER */
    if(rank == MASTER){
        MPI_Send(&result, size_vector, MPI_INT, (rank+1) % size, 1, MPI_COMM_WORLD);
        MPI_Recv(&result, size_vector, MPI_INT, size-1, 1, MPI_COMM_WORLD, &status);
        
        /* Imprime el vector original */
        printf("\nVector original: [");
        for(int i = 0; i < size_vector; i++)
            printf("%d ",vector[i]);
        printf("]\n");

        /* Imprime el resultado */
        printf("\nVector resultado: [");
        for(int i = 0; i < size_vector; i++)
            printf("%d ",result[i]);
        printf("]\n");
    }else{
        MPI_Send(&result, size_vector, MPI_INT, (rank+1) % size, 1, MPI_COMM_WORLD);
    }
    MPI_Finalize();
}