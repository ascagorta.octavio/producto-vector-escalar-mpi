#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#define MASTER 0

/*************************************************************************************************************
| - Uso:                                                                                                     |
|       Setear el valor de la variable "vector" (línea 17) y "escalar" (línea 34). Compilar usando: mpicc    |
|       <fuente> -o <ejecutable>.                                                                            |
|       Ejecutar usando: mpirun -np <nro de procesos> <ejecutable>                                           |
*************************************************************************************************************/

int main(int argc, char *argv[])

{
    /* Valor del vector*/
    int vector[] = {3,4,7,3};
    int size_vector = sizeof(vector)/sizeof(int);
    int escalar;
    int valor;
    int tag, tag_done;

    /* MPI */
    int rank, size;
    MPI_Status status;
    
    /* Inicializacion MPI */
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if(rank == MASTER)
    {   
        escalar = 5;
        tag_done =  size_vector + 1;
    }

    MPI_Bcast(&escalar, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
    MPI_Bcast(&tag_done, 1, MPI_INT, MASTER, MPI_COMM_WORLD);

    /* Proceso MASTER */
    if(rank == MASTER){
        
        int llamadas = 0;;
        int result[size_vector];

        for(int i = 0; i<size_vector; i++){

            /* Resuelve y asigna las operaciones a otros procesos */
            if( i % size == MASTER){         
                result[i] = vector[i] * escalar;
                printf("\nsoy el proceso %d y resolví el elemento %d del vector resultante, resultado: %d \n",rank,i, result[i]);
            }else{
                MPI_Send(&i, 1, MPI_INT, i % size, i, MPI_COMM_WORLD);
                llamadas++;
            }

        }
        /* Recibe las respuestas de los procesos */
        while(llamadas > 0){
            MPI_Recv(&valor, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            tag = status.MPI_TAG;
            result[tag] = valor;
            llamadas--;
        }

        /* Envia el mensaje con tag_done a los demas procesos para finalizar */ 
        for (int i = 1; i <= size-1; i++ )
            MPI_Send(&i, 1, MPI_INT, i % size, tag_done, MPI_COMM_WORLD);

        /* Imprime el vector original */
        printf("\nVector original: [");
        for(int i = 0; i < size_vector; i++)
            printf("%d ",vector[i]);
        printf("]\n");

        /* Imprime el resultado */
        printf("\nVector resultado: [");
        for(int i = 0; i < size_vector; i++)
            printf("%d ",result[i]);
        printf("]\n");

    }

    /* Procesos no MASTER */
    if(rank != MASTER){
        int index;
        for( ; ; ){
            MPI_Recv(&index, 1, MPI_INT, MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            tag = status.MPI_TAG;
            if(tag == tag_done)
                break;
            valor = vector[index] * escalar;
            printf("\nsoy el proceso %d y resolví el elemento %d del vector resultante, resultado: %d \n",rank, index, valor);
            MPI_Send(&valor, 1, MPI_INT, MASTER, index, MPI_COMM_WORLD);
        }
    }

    MPI_Finalize();
}